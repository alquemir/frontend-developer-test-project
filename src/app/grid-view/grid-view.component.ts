import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { GamesService } from '../services/games.service';
import { Game } from '../models/game';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-grid-view',
  templateUrl: './grid-view.component.html',
  styleUrls: ['./grid-view.component.css']
})

export class GridViewComponent implements OnInit {

  private _columns: number = 4;
  private _rows: number = 4;

  private readonly _gamesService: GamesService = null;
  private readonly _commonService: CommonService = null;

  public games: Game[] = [];
  public get isVisible(): boolean {
    return this._commonService.isGridViewMode;
  }

  constructor(gamesService: GamesService, commonService: CommonService) {
    this._gamesService = gamesService;
    this._commonService = commonService;

    this._gamesService.getAllGameCategories()
      .subscribe((gameCategories) => {
        this._gamesService.findGamesByCategoryId(gameCategories[0].id);
        this._commonService.toggleGridView();
      });
  }

  public get columnsRange(): number[] {
    return _.range(this._columns);
  }

  public get rowsRange(): number[] {
    return _.range(this._rows);
  }

  public getGameAt(row: number, column: number): Game {
    return this.games[row * this._columns + column];
  }


  ngOnInit() {
    this._gamesService.gamesFound$.subscribe((games: Game[]) => {
      this.games = games || [];

      if (games.length > 0) {
        this._rows = Math.round(this.games.length / this._columns) + 1;
      }
    });
  }

}
