import { Component, OnInit, Input, Output } from '@angular/core';
import { Game } from '../models/game';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-game-template',
  templateUrl: './game-template.component.html',
  styleUrls: ['./game-template.component.css']
})
export class GameTemplateComponent implements OnInit {

  private readonly _commonService: CommonService = null;

  @Input()
  public game: Game = null;

  public get isGridViewMode(): boolean {
    return this._commonService.isGridViewMode;
  }

  constructor(commonService: CommonService) {
    this._commonService = commonService;
  }

  ngOnInit() {
  }

}
