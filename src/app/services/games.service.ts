import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Game } from '../models/game';
import { GameCategory } from '../models/game-category';
import { SortBy } from '../models/sort-by';
import { KMP } from '../common/kmp';
import 'rxjs';

@Injectable()
export class GamesService {

  private readonly _gamesListUrl = '../../../data/games-list.json';
  private readonly _categoriesListUrl = '../../data/categories-list.json';
  private readonly _http: Http = null;
  private _cachedGames: Game[] = [];

  private readonly _gamesFoundSource: any = null;
  private readonly _gamesFound$: any = null;


  constructor(http: Http) {
    this._http = http;
    this._gamesFoundSource = new Subject();
    this._gamesFound$ = this._gamesFoundSource.asObservable();
  }

  public get gamesFound$() {
    return this._gamesFound$;
  }


  public findGamesByCategoryId(categoryId: number): void {
    this._http.get(this._gamesListUrl)
      .map((res) => this.extractGamesFromHttpResponse(res))
      .map((games) => this.filterGamesByCategoryId(games, categoryId))
      .catch((error) => this.handleError(error))
      .subscribe((games) => {
        this._cachedGames = games;
        this._gamesFoundSource.next(this._cachedGames);
      });

  }

  public getAllGameCategories(): Observable<GameCategory[]> {
    return this._http.get(this._categoriesListUrl)
      .map((gameCategories) => this.extractGameCategoriesFromHttpResponse(gameCategories))
      .catch((error) => this.handleError(error));
  }

  public findGames(keywords: string): void {
    if (keywords.length == 0) return;

    this._http.get(this._gamesListUrl)
      .map((res) => this.extractGamesFromHttpResponse(res))
      .map((games) => this.filterGamesByName(games, keywords))
      .catch((error) => this.handleError(error))
      .subscribe((games) => {
        this._cachedGames = games;
        this._gamesFoundSource.next(this._cachedGames);
      });
  }


  public sortGames(sortBy: SortBy): void {
    let sortedGames = this.sortGamesBy(this._cachedGames, sortBy);;
    this._gamesFoundSource.next(sortedGames);
  }

  private extractGameCategoriesFromHttpResponse(res: Response): GameCategory[] {
    let json = res.json();

    return json != null
      ? json.map((data: any) => this.extractGameCategoryFromJsonData(data))
      : [];
  }

  private extractGameCategoryFromJsonData(data: any): GameCategory {
    return data != null
      ? (() => {
        let gameCategory = new GameCategory();

        gameCategory.id = data.id;
        gameCategory.name = data.displayName;

        return gameCategory;
      })()
      : new GameCategory();
  }

  private filterGamesByCategoryId(games: Game[], categoryId: number): Game[] {
    return games
      .filter((game: any) => this.filterGameByCategoryId(game, categoryId));
  }

  private filterGameByCategoryId(game: Game, categoryId: number): boolean {
    return game.categories.some((category) => category.id == categoryId);
  }

  private filterGamesByName(games: Game[], name: string): Game[] {
    return games
      .filter((game: any) => this.filterGameByName(game, name));
  }

  private filterGameByName(game: Game, name: string): boolean {
    let index = KMP.search(game.name, name);
    return index != game.name.length;
  }

  private sortGamesBy(games: Game[], sortBy: SortBy): Game[] {
    if (sortBy == SortBy.Alphabetically) {
      return this.sortGamesAlphabetically(games);
    }

    if (sortBy == SortBy.Popularity) {
      return this.sortGamesByPopularity(games);
    }

    return null;
  }

  private sortGamesAlphabetically(games: Game[]): Game[] {
    return games.sort((a, b) => {
      return a.name == b.name ? 0 : a.name < b.name ? -1 : 1;
    });
  }

  private sortGamesByPopularity(games: Game[]): Game[] {
    return games.sort((a, b) => {
      let orderNumberA = this.getHighestOrderNumberFromGameCategories(a);
      let orderNumberB = this.getHighestOrderNumberFromGameCategories(b);

      return orderNumberA == orderNumberB ? 0 : orderNumberA < orderNumberB ? -1 : 1;
    });
  }

  private getHighestOrderNumberFromGameCategories(game: Game): number {
    return Math.max.apply(Math, game.categories.map((category) => {
      return category.orderNumber;
    }));
  }

  private extractGamesFromHttpResponse(res: Response): Game[] {
    let json = res.json();
    return json != null
      ? json.map((data: any) => this.extractGameFromJsonData(data))
      : [];
  }

  private extractGameCategoriesData(categories: any): GameCategory[] {
    return categories != null
      ? (() => {
        return categories.map((category: any) => {
          let gameCategory = new GameCategory();

          gameCategory.id = category.categoryId || 0;
          gameCategory.orderNumber = category.orderNumber || 0;

          return gameCategory;
        });
      })()
      : [];
  }

  private extractGameFromJsonData(data: any): Game {
    return data != null
      ? (() => {
        let game = new Game();

        game.id = data.gameId || 0;
        game.vendorId = data.vendorId || 0;
        game.name = data.name || '';
        game.slug = data.slug || '';
        game.imageUrl = this.getGameImageUrl(data.gameId);
        game.categories = this.extractGameCategoriesData(data.categories);

        return game;
      })()
      : new Game();
  }

  private getGameImageUrl(gameId: number): string {
    return `https://assets.igamingcloud.com/game-icons/${gameId}.jpg`;
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);

    return Observable.throw(errMsg);
  }
}
