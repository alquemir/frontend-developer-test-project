
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CommonService {

  private _toggleGridViewSource: any = null;
  private _toggleGridView$: any = null;

  private _toggleListViewSource: any = null;
  private _toggleListView$: any = null;

  private _isGridViewMode: boolean = false;

  public get isGridViewMode(): boolean {
    return this._isGridViewMode;
  }

  constructor() {
    this._toggleGridViewSource = new Subject();
    this._toggleGridView$ = this._toggleGridViewSource.asObservable();

    this._toggleListViewSource = new Subject();
    this._toggleListView$ = this._toggleListViewSource.asObservable();
  }

  public get toggleGridView$(): any {
    return this._toggleGridView$;
  }

  public get toggleListView$(): any {
    return this._toggleListView$;
  }

  public toggleGridView(): void {
    this._isGridViewMode = true;
    this._toggleGridViewSource.next();
  }

  public toggleListView(): void {
    this._isGridViewMode = false;
    this._toggleListViewSource.next();
  }
}
