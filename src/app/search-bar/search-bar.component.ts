import { Component, OnInit } from '@angular/core';
import { GamesService } from '../services/games.service';
import { SortBy } from '../models/sort-by';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent implements OnInit {

  public keywords: string = '';
  private _gamesService: GamesService = null;

  constructor(gamesService: GamesService) {
    this._gamesService = gamesService;
  }

  ngOnInit() {

  }

  search(sortBy: SortBy) {
    this._gamesService.findGames(this.keywords);
  }

  setSearchKeywords(e: Event) {
    this._gamesService.findGames(this.keywords);
  }
}
