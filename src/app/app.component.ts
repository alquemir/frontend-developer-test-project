import { Component } from '@angular/core';
import { GamesService } from './services/games.service';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [GamesService, CommonService]
})

export class AppComponent {
  title = 'app works!'
}
