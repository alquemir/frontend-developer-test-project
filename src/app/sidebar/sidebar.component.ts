import { Component, OnInit } from '@angular/core';
import { GamesService } from '../services/games.service';
import { GameCategory } from '../models/game-category';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {

  private readonly _gamesService: GamesService = null;

  public gameCategories: GameCategory[] = [];

  constructor(gamesService: GamesService) {
    this._gamesService = gamesService;
    this._gamesService.getAllGameCategories()
      .subscribe((gameCategories) => {
        this.gameCategories = gameCategories;
      });

  }

  getGamesByCategory(categoryId: number) {
    this._gamesService.findGamesByCategoryId(categoryId);
  }

  ngOnInit() {
  }

}
