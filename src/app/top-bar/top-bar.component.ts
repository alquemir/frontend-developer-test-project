import { Component, OnInit } from '@angular/core';
import { SearchBarComponent } from '../search-bar/search-bar.component';
import { SortBy } from '../models/sort-by';
import { GamesService } from '../services/games.service';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})

export class TopBarComponent implements OnInit {

  private readonly _gamesService: GamesService = null;
  private readonly _commonService: CommonService = null;

  public get isGridViewMode(): boolean {
    return this._commonService.isGridViewMode;
  }

  constructor(gamesService: GamesService, commonService: CommonService) {
    this._commonService = commonService;
    this._gamesService = gamesService;
  }

  sortByPopularity(e: Event) {
    this._gamesService.sortGames(SortBy.Popularity);
  }

  sortAlphabetically(e: Event) {
    this._gamesService.sortGames(SortBy.Alphabetically);
  }

  toggleGridView(e: Event) {
    this._commonService.toggleGridView();
  }

  toggleListView(e: Event) {
    this._commonService.toggleListView();
  }

  ngOnInit() {
  }

}
