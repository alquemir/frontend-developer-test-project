import { GameCategory } from './game-category';

export class Game {
  public id: number = 0;
  public vendorId: number = 0;
  public imageUrl: string = '';
  public name: string = '';
  public slug: string = '';
  public categories: GameCategory[] = [];
}
