export class GameCategory {
  public id: number = 0;
  public name: string = '';
  public orderNumber: number = 0;
}
