
export class KMP {

  private static sortAlphabet(input: string): string[] {
    return Array.from(input).sort();
  }

  private static extractAlphabet(input: string): any {
    let lookup = {};

    let alphabet = this.sortAlphabet(input);
    alphabet.forEach((ch) => lookup[ch] = [0]);

    return lookup;
  }

  private static buildDFA(pattern: string): any {
    let dfa = {};

    dfa = this.extractAlphabet(pattern);
    dfa[pattern.charAt(0)][0] = 1;

    for (let X = 0, j = 1; j < pattern.length; j++) {

      Object.keys(dfa).forEach((ch: string) => {
        dfa[ch][j] = dfa[ch][X]
      });

      dfa[pattern.charAt(j)][j] = j + 1;
      X = dfa[pattern.charAt(j)][X];
    }

    return dfa;
  }

  public static search(text: string, pattern: string): number {
    let i, j;

    text = text.toLowerCase();
    pattern = pattern.toLowerCase();

    let dfa = this.buildDFA(pattern);
    let N = text.length;
    let M = pattern.length;

    for (i = 0, j = 0; i < N && j < M; i++) {
      if (dfa[text.charAt(i)] != null) {
        j = dfa[text.charAt(i)][j];
      }
    }

    return j == M ? i - M : N;
  }
}
