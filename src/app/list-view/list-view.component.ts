import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { GamesService } from '../services/games.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {

  private readonly _gamesService: GamesService = null;
  private readonly _commonService: CommonService = null;

  public games: Game[] = [];
  public get isVisible(): boolean {
    return !(this._commonService.isGridViewMode);
  }

  constructor(gamesService: GamesService, commonService: CommonService) {
    this._gamesService = gamesService;
    this._commonService = commonService;
  }

  ngOnInit() {

    this._gamesService.gamesFound$.subscribe((games: Game[]) => {
      this.games = games || [];
    });
  }

}
