import { KMP } from '../app/common/kmp';

describe('Test the local implementation of the KMP algorithm', () => {
  it('should do something', () => {
    expect(KMP.search('zeababacxx', 'ababac')).toBe(2);
    expect(KMP.search('zealabacxx', 'ababac')).toBe(10);
    expect(KMP.search('hello world message', 'world')).toBe(6);
  });
});
