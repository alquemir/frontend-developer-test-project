# Frontend Developer Test Project

## Development server

* Run `npm install` to install all local npm dev dependencies
* Run `npm run serve` for a dev server.
* Navigate to `http://localhost:8080/`.
* The app will automatically reload if you change any of the source files.
