import { FrontendDeveloperTestProjectPage } from './app.po';

describe('frontend-developer-test-project App', () => {
  let page: FrontendDeveloperTestProjectPage;

  beforeEach(() => {
    page = new FrontendDeveloperTestProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
