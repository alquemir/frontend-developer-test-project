const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');


module.exports = {
  entry: {
    app: path.resolve(__dirname, 'src/main.ts'),
    polyfills: path.resolve(__dirname, 'src/polyfills.ts')
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js', '.less', '.css']
  },
  bail: true,
  module: {
    rules: [
      {
	test: /\.ts$/,
	use: [
	  {
	    loader: 'awesome-typescript-loader',
	    options: {
	      configFileName: path.resolve(__dirname, 'tsconfig.json')
	    }
	  },
	  {
	    loader: 'angular2-template-loader'
	  }
	]
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=assets/[name].[hash].[ext]'
      },
      {
        test: /\.css$/,
        exclude: path.resolve(__dirname, 'src/app'),
        use: ExtractTextPlugin.extract({
	  fallback: 'style-loader',
	  use: ['css-to-string-loader', 'css-loader?sourceMap']
	})
      },
      {
	test: /\.css$/,
	include: path.resolve(__dirname, 'src/app'),
	loader: 'raw-loader'
      },
      {
	test: /\.less$/,
	exclude: path.resolve(__dirname, 'src/app'),
	use: ExtractTextPlugin.extract({
	  fallback: 'style-loader',
	  use: ['css-to-string-loader', 'css-loader?sourceMap', 'less-loader']
	})
      }
    ]
  },
  plugins: [
    // Workaround for angular/angular#11580
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
	/angular(\\|\/)core(\\|\/)@angular/,
      path.resolve(__dirname, 'src/app'), // location of your src
      {} // a map of your routes
    ),

    new webpack.optimize.CommonsChunkPlugin({
      name: ['app', 'vendor', 'polyfills']
    }),

    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),

    new ExtractTextPlugin('[name].css')
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[id].chunk.js'
  },
  devServer: {
    historyApiFallback: true,
    stats: 'minimal'
  }
  
};
